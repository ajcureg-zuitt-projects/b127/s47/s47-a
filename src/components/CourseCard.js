import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';


// export default function CourseCard(props) {

// 	// Checks to see if the data was successfully passed
// 	console.log(props)
// 	console.log(typeof props)

// 	return(
// 		<Card>
// 			<Card.Body>
// 				<Card.Title><h4>{props.courseProp.name}</h4></Card.Title>
// 				<Card.Subtitle><h5>Description:</h5></Card.Subtitle>

// 				<Card.Text>
// 						{props.courseProp.description}
// 				</Card.Text>

// 				<Card.Subtitle><h5>Price:</h5></Card.Subtitle>

// 				<Card.Text>
// 					{props.courseProp.price}
// 				</Card.Text>

// 				<Button variant="primary">Enroll</Button>
// 			</Card.Body>
// 		</Card>	
// 	)
// }

// export default function CourseCard({courseProp}) {

// // Deconstruct the course properties into their own variables (destructing)
// 	return(
// 		<Card>
// 			<Card.Body>
// 				<Card.Title><h4>{courseProp.name}</h4></Card.Title>
// 				<Card.Subtitle><h5>Description:</h5></Card.Subtitle>

// 				<Card.Text>
// 						{courseProp.description}
// 				</Card.Text>

// 				<Card.Subtitle><h5>Price:</h5></Card.Subtitle>

// 				<Card.Text>
// 					{courseProp.price}
// 				</Card.Text>
				
// 				<Button variant="primary">Enroll</Button>
// 			</Card.Body>
// 		</Card>	
// 	)
// }

export default function CourseCard({courseProp}) {

// Deconstruct the course properties into their own variables (destructing)
const { name, description, price } = courseProp;

// Use the state hook for this component to be able to store its state
// States are used to keep track of information related to individual component
// Syntax
	// const [getter, setter] = useState(initialGetterValue)
	// getter = stored initial(default value)
	// setter = updated value

const [count,setCount] = useState(0)
const [seats,setSeats] = useState(10)

// state hook that indicates the button enrollment availability
const [isOpen, setisOpen] = useState(true);

useEffect(() => {
	if(seats === 0) {
		setisOpen(false);
	}
}, [seats])

// console.log(useState(0))

	// const enroll = () => {
	// 	if (seat<=0) {
	// 		alert('No more seats available.');
	// 	}
	// 	else {
	// 		setCount(count + 1);
	// 		setSeat(seat - 1);
	// 	}
	// }


function enroll(){
	if (seats > 0 ){
		setCount(count + 1);
		console.log('Enrollees: ' + count);
		setSeats(count - 1);
		console.log('Seats: ' + seats);	
	}else{
		alert('No more seats available.');
	}
}

	return(
		<Card>
			<Card.Body>
				<Card.Title><h4>{name}</h4></Card.Title>
				<Card.Subtitle><h5>Description:</h5></Card.Subtitle>

				<Card.Text>
						{description}
				</Card.Text>

				<Card.Subtitle><h5>Price:</h5></Card.Subtitle>

				<Card.Text> Php {price} </Card.Text>
				<Card.Text> Enrollees: {count} </Card.Text>
				<Card.Text> Seats: {seats} </Card.Text>

				{isOpen ?
				<Button variant="primary" onClick={enroll}>Enroll</Button>
				:
				<Button variant="primary" disabled>Enroll</Button>
				}
			</Card.Body>
		</Card>	
	)
}


// Check if the CourseCard Component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.
CourseCard.propTypes = {
	// The 'shape' method is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		// define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
