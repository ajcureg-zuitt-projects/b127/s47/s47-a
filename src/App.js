import { Fragment } from 'react';

import './App.css';
import AppNavbar from './components/AppNavbar';

// pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
// import Counter from './components/Counter';
import Login from './pages/Login'

// Bootstrap
import { Container } from 'react-bootstrap' ;

function App() {
  return (
    <Fragment>
      < AppNavbar />
      <Container>
        {/*<Counter/>*/}
        <Login />
        <Register />
        <Home/>
        <Courses/>
      </Container>
    </Fragment>
  );
}

export default App;

/*
NOTES:

With the React Fragment component, we can group multiple components and avoid adding extra code

<Fragment> is prefrerred over <></> (shortcut syntax) because it is not universal and can cause problems in some other editors

JSX Syntax
JSX, or Javascript XML is an extension to the syntax of JS. It allowas us to write HTML-like syantax within out React js projects and it includes JS features as well.

Install the JS(Babel) linting for code readability
1. Ctrl + Shift + P
2. In the input field, type the word "install" and select the "Package Control: Install Package" option to trigger an installation of an add-on feature
3. Type "Babel" in the input field to be installed
*/